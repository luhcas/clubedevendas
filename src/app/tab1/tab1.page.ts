import { Component } from '@angular/core';
import { ClubeService } from '../clube.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})

export class Tab1Page {
  public lista_empresas = new Array<any>();

  constructor(
    private ClubeService: ClubeService
  ) {
    this.ClubeService.getEmpresas().subscribe(
      data => {
        this.lista_empresas = data;
        //console.log(this.lista_empresas);
      },
      error => {
        console.log(error);
      }
    )
  }
}
