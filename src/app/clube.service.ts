import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/Storage';

@Injectable({
  providedIn: 'root'
})
export class ClubeService {
  private baseApiPath = "http://localhost:80/clube/index.php/";
  private key = "$2y$10AmWrsgylokRon1HjZ45tTOqb9hndoYVxdmDbwPlFb02kZRg4RXux2";
  private codigo = '654321';
  //private codigo:any = [];

  constructor(private http: HttpClient, private storage: Storage) {
    //this.getPerfil();
  }

  /*async getPerfil(){
    const val = await this.storage.get('user_codigo');
    if (val) {
      console.log(val);
      this.codigo = [0, val];
      return await this.http.get<Array<any>>(this.baseApiPath + "api/fetch_perfil/?codigo=" + val);
    }
  }*/

  getEmpresas(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/?key=" + this.key);
  }

  getCompras(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/fetch_compras/?codigo=" + this.codigo);
  }

  getSaques(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/fetch_saques/?codigo=" + this.codigo);
  }

  getProcessando(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/fetch_processando/?codigo=" + this.codigo);
  }

  getDisponivel(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/fetch_disponivel/?codigo=" + this.codigo);
  }

  getRetirado(){
    return this.http.get<Array<any>>(this.baseApiPath + "api/fetch_retirado/?codigo=" + this.codigo);
  }
}

@Injectable({
  providedIn: 'root'
})
export class PostProvider {
	server: string = "http://localhost:80/clube/index.php/api/proses_api/"; // default
	// if you test in real device "http://localhost" change use the your IP
    // server: string = "http://192.199.122.100/IONIC4_CRUD_LOGINREGIS_PHP_MYSQL/server_api/";

	constructor(public http : Http) {

	}

	postData(body, file){
		let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
		.map(res => res.json());
	}
}
