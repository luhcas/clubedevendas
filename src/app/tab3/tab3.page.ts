import { Component } from '@angular/core';
import { PerfilService } from './perfil.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  public perfil = new Array<any>();

  constructor(
    private PerfilService: PerfilService
  ) {

    this.PerfilService.getPerfil().subscribe(
      data=>{
        let perfil;
        perfil = data;
        this.perfil = data;
        console.log(data);
      },
      error => {
        //console.log(error);
      }
    )

    /*this.PerfilService.getPerfil().subscribe(
      data => {
        this.perfil = data;
        console.log(this.perfil);
      },
      error => {
        console.log(error);
      }
    )*/
  }

}
