import { Component } from '@angular/core';
import { ClubeService } from '../clube.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  public lista_compras = new Array<any>();
  public lista_saques = new Array<any>();
  public valor_processando = new Array<any>();
  public valor_disponivel = new Array<any>();
  public valor_retirado = new Array<any>();

  constructor(
    private ClubeService: ClubeService
  ) {
    this.ClubeService.getCompras().subscribe(
      data => {
        this.lista_compras = data;
        console.log(this.lista_compras);
      },
      error => {
        console.log(error);
      }
    ),
    this.ClubeService.getSaques().subscribe(
      data => {
        this.lista_saques = data;
        console.log(this.lista_saques);
      },
      error => {
        console.log(error);
      }
    ),
    this.ClubeService.getProcessando().subscribe(
      data => {
        this.valor_processando = data;
        console.log(this.valor_processando);
      },
      error => {
        console.log(error);
      }
    ),
    this.ClubeService.getDisponivel().subscribe(
      data => {
        this.valor_disponivel = data;
        console.log(this.valor_disponivel);
      },
      error => {
        console.log(error);
      }
    ),this.ClubeService.getRetirado().subscribe(
      data => {
        this.valor_retirado = data;
        console.log(this.valor_retirado);
      },
      error => {
        console.log(error);
      }
    )
  }

}
